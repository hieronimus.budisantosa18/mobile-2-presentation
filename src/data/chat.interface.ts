export interface Chat{
    id: string;
    person: string;
    text: string;
}