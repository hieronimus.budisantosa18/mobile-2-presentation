export interface Doctor{
    id: number;
    name: string;
    gender: string;
    email: string;
    phone_number: string;
    specialicst : string;
    signature: Date;

}