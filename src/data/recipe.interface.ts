export interface Recipe{
    id: number;
    doctor_id : number;
    patient_id: number;
    pharmacist_id: number;
    doctor_name : string;
    patient_name: string;
    pharmacist_name: string;
    description: string;
    created_at: string
    email:string;
}