export interface Dose{
    id: number;
    min_dose: number;
    max_dose: number;
    unit: string;
    min_age: number;
    max_age: number;
    time_1: number;
    time_unit_1: string;
    time_2:number;
    time_unit_2:string;
    category_id:number;
    doseString:string;
}