export interface Account{
    id: number;
    name: string;
    gender: string;
    email: string;
    phone_number: string;
    signature: Date;

}