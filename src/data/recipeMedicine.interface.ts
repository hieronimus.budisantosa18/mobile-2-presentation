export interface RecipeMedicine{
    id: number;
    name : string;
    doses: string;
    status: number;
    description : string;
    quantity: number;
    medicine_id: number;
    recipe_id : number;
    alarm_1: string;
    alarm_2: string;
    alarm_3: string;
    alarm_desc_1: string;
    alarm_desc_2: string;
    alarm_desc_3: string;
}