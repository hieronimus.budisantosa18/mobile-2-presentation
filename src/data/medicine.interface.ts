export interface Medicine{
    id: number;
    name: string;
    generic_name: string;
    summary: string;
    description: string;
    side_effect: string;
    image: string;
    price: number;
    category_id:number;
    amount:number;
    unit:string;

}