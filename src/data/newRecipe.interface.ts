export interface NewRecipe{
    patient_id: number;
    pharmacist_id: number;
    description: string;
    created_at: Date;
    patient_age : number;
}