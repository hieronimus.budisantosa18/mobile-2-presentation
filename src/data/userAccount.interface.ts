export interface UserAccount {
    id: number;
    name: string;
    email: string;
    type: string;
}