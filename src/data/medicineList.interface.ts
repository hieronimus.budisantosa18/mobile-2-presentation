export interface MedicineList{
    id: number;
    name: string;
    doses: string;
    status: string;
    description: string;
    quantity: number;
    medicine_id: number;
    recipe_id: number;
    alarm_1:number;
    alarm_2:number;
    alarm_3:number;
    alarm_desc_1: string;
    alarm_desc_2: string;
    alarm_desc_3: string;
}