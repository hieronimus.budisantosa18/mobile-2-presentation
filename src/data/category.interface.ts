export interface Category{
    id: number;
    name: string;
    usage: string;
    prohibition: string;
    info: string;
}