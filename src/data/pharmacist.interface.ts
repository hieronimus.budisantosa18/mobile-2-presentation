export interface Pharmacist{
    id: number;
    name: string;
    gender: string;
    email: string;
    phone_number: string;
    SIK: string;
}