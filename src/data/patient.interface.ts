export interface Patient{
    id: number;
    name: string;
    email: string;
    phone_number: string;
    address: string;
    birth_date: string;
    gender: string;
}