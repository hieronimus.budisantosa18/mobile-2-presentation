import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from "@angular/http"; 
import { LocalNotifications } from "@ionic-native/local-notifications";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from "../pages/tabs/tabs";
import { DaftarResepPage } from "../pages/daftar-resep/daftar-resep";
import { InfoResepPage } from "../pages/info-resep/info-resep";
import { DaftarPasienPage } from "../pages/daftar-pasien/daftar-pasien";
import { NamaPasienPage } from "../pages/nama-pasien/nama-pasien";
import { TambahPasienPage } from "../pages/tambah-pasien/tambah-pasien";
import { BiodataPage } from "../pages/biodata/biodata";
import { FormIsiResepPage } from "../pages/form-isi-resep/form-isi-resep";
import { FormIsiObatPage } from "../pages/form-isi-obat/form-isi-obat";
import { DaftarObatPage } from "../pages/daftar-obat/daftar-obat";
import { ChattingPage } from "../pages/chatting/chatting";
import { SignInPage } from "../pages/sign-in/sign-in";
import { ModalDetailResepObatPage } from "../pages/modal-detail-resep-obat/modal-detail-resep-obat";
import { ModalDetailResepPage } from "../pages/modal-detail-resep/modal-detail-resep";
import { ModalDosisObatPage } from "../pages/modal-dosis-obat/modal-dosis-obat";
import { FormEditObatPage } from "../pages/form-edit-obat/form-edit-obat";
import { FormEditResepPage } from "../pages/form-edit-resep/form-edit-resep";
import { AlarmPage } from "../pages/alarm/alarm";
import { AlarmListPage } from "../pages/alarm-list/alarm-list";
import { AllAlarmPage } from "../pages/all-alarm/all-alarm";
import { ModalSetAlarmPage } from "../pages/modal-set-alarm/modal-set-alarm";
import { ModalEditAlarmPage } from "../pages/modal-edit-alarm/modal-edit-alarm";
import { AboutPage } from "../pages/about/about";

//Service Start
import { AuthService } from "../services/auth";
import { ChattingService } from "../services/chatting";
import { UserService } from '../services/users';
import { AlarmService } from "../services/alarms";
import { RecipeService } from "../services/recipes"
import { MedicineService } from "../services/medicines";
import { DoctorService } from "../services/doctors";
import { PatientService } from "../services/patients";
import { PharmacistService } from "../services/pharmacists";
import { MedicineListService } from "../services/medicinelists";
import { DoseService } from "../services/doses";
import { CategoryService } from "../services/categories";
import { CreateRecipeService } from "../services/createRecipe";


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database-deprecated';

var config = {
  apiKey: "AIzaSyBpHGvoSaPHl4ENt_P8FLWhbqGzvTxMh9k",
  authDomain: "e-recipe.firebaseapp.com",
  databaseURL: "https://e-recipe.firebaseio.com",
  projectId: "e-recipe",
  storageBucket: "e-recipe.appspot.com",
  messagingSenderId: "944568758650"}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    DaftarResepPage,
    InfoResepPage,
    DaftarPasienPage,
    NamaPasienPage,
    TambahPasienPage,
    BiodataPage,
    FormIsiResepPage,
    FormIsiObatPage,
    ChattingPage,
    SignInPage,
    DaftarObatPage,
    ModalDetailResepObatPage,
    ModalDetailResepPage,
    ModalDosisObatPage,
    FormEditObatPage,
    FormEditResepPage,
    AlarmPage,
    AlarmListPage,
    AllAlarmPage,
    ModalSetAlarmPage,
    ModalEditAlarmPage,
    AboutPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,

    DaftarResepPage,
    InfoResepPage,
    DaftarPasienPage,
    NamaPasienPage,
    TambahPasienPage,
    BiodataPage,
    FormIsiResepPage,
    FormIsiObatPage,
    ChattingPage,
    SignInPage,
    DaftarObatPage,
    ModalDetailResepObatPage,
    ModalDetailResepPage,
    ModalDosisObatPage,
    FormEditObatPage,
    FormEditResepPage,
    AlarmPage,
    AlarmListPage,
    AllAlarmPage,
    ModalSetAlarmPage,
    ModalEditAlarmPage,
    AboutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    ChattingService,
    UserService,
    AlarmService,
    RecipeService,
    MedicineService,
    DoctorService,
    PatientService,
    MedicineListService,
    PharmacistService,
    DoseService,
    CategoryService,
    CreateRecipeService,
    LocalNotifications,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
