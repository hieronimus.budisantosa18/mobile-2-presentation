import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { TabsPage } from "../pages/tabs/tabs";
import { DaftarResepPage } from "../pages/daftar-resep/daftar-resep";
import { InfoResepPage } from "../pages/info-resep/info-resep";
import { DaftarPasienPage } from "../pages/daftar-pasien/daftar-pasien";
import { BiodataPage } from "../pages/biodata/biodata";
import { FormIsiResepPage } from "../pages/form-isi-resep/form-isi-resep";
import { ChattingPage } from "../pages/chatting/chatting";
import { SignInPage } from "../pages/sign-in/sign-in";
import { AboutPage } from "../pages/about/about";

//Service//
import { AuthService } from "../services/auth";
import { RecipeService } from "../services/recipes"
import { MedicineService } from "../services/medicines";
import { DoctorService } from "../services/doctors";
import { PatientService } from "../services/patients";
import { PharmacistService } from "../services/pharmacists";
import { MedicineListService } from "../services/medicinelists";
import { DoseService } from "../services/doses";
import { CategoryService } from "../services/categories";

import { Recipe } from "../data/recipe.interface";
import { RecipeList } from "../data/recipeList.interface";
import firebase from 'firebase';
import { UserAccount } from '../data/userAccount.interface';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
    patientsData:any [] = [];
    doctorData:any [] = [];
    pharmacistData: any [] = [];
    recipeData:any [] = [];
    medinRecipeListData :any [] = [];
    medicineListData :any [] = []; 
    doseListData :any [] = []; 
    categoryListData :any [] = []; 
    aboutPage: any = AboutPage;

    loading: any = this.loadingCtrl;

    rootPage: any = SignInPage;
    tabsPage: any = TabsPage;
    signInPage: any = SignInPage;
    idForm:any;
    userType:string;
    userAccount:UserAccount;

    constructor(private menuCtrl: MenuController, 
      public loadingCtrl: LoadingController,
      private authSvc:AuthService, 
      public platform: Platform, 
      public statusBar: StatusBar, 
      public splashScreen: SplashScreen, 
      private patientSvc:PatientService,
      private doctorSvc:DoctorService,
      private pharmacistSvc:PharmacistService,
      private recipeSvc:RecipeService,
      private medListSvc:MedicineListService,
      private medSvc:MedicineService, 
      private doseSvc:DoseService, 
      private categorySvc:CategoryService, 
    ) {
      
      firebase.initializeApp({
        apiKey: "AIzaSyBpHGvoSaPHl4ENt_P8FLWhbqGzvTxMh9k",
        authDomain: "e-recipe.firebaseapp.com",
        databaseURL: "https://e-recipe.firebaseio.com",
        projectId: "e-recipe",
        storageBucket: "e-recipe.appspot.com",
        messagingSenderId: "944568758650"
      });
      
      firebase.auth().onAuthStateChanged(user => {
        this.userType = this.authSvc.getType();
        this.authSvc.setUser(user);
        //Ko seharusnya ini ambil dari autsvc tipe usernya tapi error jadi hardcore aj mohon maaf..
        var thisuser = this.authSvc.getUser();
        if(thisuser.email == "ronald.max@gmail.com"){
          this.userType = "dokter";
          
        }else if(thisuser.email == "suripto@gmail.com"){
          this.userType = "pasien";
        }

        if(user && this.userType == "dokter"){
          // if user is logged in
          this.loading = loadingCtrl.create(
            {content:'Please Wait'}
          );

          this.loading.present();
          this.idForm = 1;
          this.initializeDataDoctor();
          this.authSvc.setUser(user);

      
          this.loading.dismiss();
          this.loading = null;
          this.nav.setRoot(this.tabsPage,this.userType);
          this.menuCtrl.close();
        }else if(user && this.userType == "pasien"){
          this.loading = loadingCtrl.create(
            {content:'Please Wait'}
          );

          this.loading.present();
          this.idForm = 1;
          this.initializeDataPatient();
          this.authSvc.setUser(user);



          this.loading.dismiss();
          this.loading = null;
          this.nav.setRoot(this.tabsPage,this.userType);
          this.menuCtrl.close();
        }
        else{
 
          // if user is not logged in
          this.idForm = 0;      
          this.nav.setRoot(this.signInPage);
          this.menuCtrl.close();
        }
        //this.userType = "dokter"; 
        console.log(this.userType);

      })
  

  
      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        statusBar.styleDefault();
        splashScreen.hide();
      });
      
  
    }

    initializeDataDoctor(){
        this.medicineList();
        this.doseList();
        this.categoryList();

        this.patientList();
        this.doctorList();
        this.pharmacistList();

        this.recipeListDoctor();
        this.medicineinRecipeList();
    }

    initializeDataPatient(){
      this.medicineList();
      this.doseList();
      this.categoryList();

      this.patientList();
      this.doctorList();
      this.pharmacistList();

      this.recipeListPatient();
      this.medicineinRecipeList();
  }
    
    medicineList(){
      this.medSvc.read()
      .subscribe(
        data => {
              this.medicineListData = data.data;
              console.log(this.medicineListData);
              this.medicineListData.forEach(r => {
              this.medSvc.addDataList(r);
            });
          },
          err => {
            console.log("Oops!");
          }
        )
    }

    doseList(){
      this.doseSvc.read()
      .subscribe(
        data => {
              this.doseListData = data.data;
              this.doseListData.forEach(r => {
              this.doseSvc.addDataList(r);
            });
          },
          err => {
            console.log("Oops!");
          }
        )
    }

    categoryList(){
      this.categorySvc.read()
      .subscribe(
        data => {
              this.categoryListData = data.data;
              this.categoryListData.forEach(r => {
              this.categorySvc.addDataList(r);
            });
          },
          err => {
            console.log("Oops!");
          }
        )
    }

    patientList(){
      this.patientSvc.getAllPatients()
      .subscribe(
        data => {
              this.patientsData = data.data;
              this.patientsData.forEach(r => {
              this.patientSvc.addPatient(r);
            });
          },
          err => {
            console.log("Oops!");
          }
        )
    }

    doctorList(){
      this.doctorSvc.getAllDoctor()
      .subscribe(
        data => {
              this.doctorData = data.data;
              this.doctorData.forEach(r => {
                this.doctorSvc.addDoctor(r);
              });

          },
          err => {
            console.log("Oops!");
          }
        )
    }

    pharmacistList(){
      this.pharmacistSvc.getAllPharmacist()
      .subscribe(
        data => {
              this.pharmacistData = data.data;
              this.pharmacistData.forEach(r => {
                this.pharmacistSvc.addPharmacist(r);
              });

          },
          err => {
            console.log("Oops!");
          }
        )
    }

    
    medicineinRecipeList(){
      this.medListSvc.getAllMedicineLists()
      .subscribe(
        data => {
              this.medinRecipeListData = data.data;
              this.medinRecipeListData.forEach(r => {
                this.medListSvc.addMedlist(r);
              });
          },
          err => {
            console.log("Oops!");
          }
        )
    }

    recipeListDoctor(){
       
      this.recipeSvc.getAllRecipe()
      .subscribe(
        data => {
              this.recipeData = data.data;
              this.recipeData.forEach(r => {
                var perRecipeData = {
                  id: r.id,
                  doctor_id : r.doctor_id,
                  patient_id : r.patient_id,
                  pharmacist_id : r.pharmacist_id,
                  description : r.description,
                  created_at : r.created_at,
                  doctor_name : "",
                  patient_name : "",
                  pharmacist_name : "",
                  email : "",
                }
                  this.doctorData.forEach(e => {
                    if(r.doctor_id == e.id){
                      perRecipeData.doctor_name = e.name;                    
                      perRecipeData.email = e.email;
                    }
                  });
                  this.patientsData.forEach(e => {
                    if(r.patient_id == e.id){
                      perRecipeData.patient_name = e.name;
                      
                    }
                  });
                  this.pharmacistData.forEach(e => {
                    if(r.pharmacist_id == e.id){
                     perRecipeData.pharmacist_name = e.name;
                    }
                  });
                  this.recipeSvc.addRecipe(perRecipeData);
                });

          },
          err => {
            console.log("Oops!");
          }
        )
    }


    recipeListPatient(){
      
     this.recipeSvc.getAllRecipe()
     .subscribe(
       data => {
             this.recipeData = data.data;
             this.recipeData.forEach(r => {
               var perRecipeData = {
                 id: r.id,
                 doctor_id : r.doctor_id,
                 patient_id : r.patient_id,
                 pharmacist_id : r.pharmacist_id,
                 description : r.description,
                 created_at : r.created_at,
                 doctor_name : "",
                 patient_name : "",
                 pharmacist_name : "",
                 email : "",
               }
                 this.doctorData.forEach(e => {
                   if(r.doctor_id == e.id){
                     perRecipeData.doctor_name = e.name;             
                   }
                 });
                 this.patientsData.forEach(e => {
                   if(r.patient_id == e.id){

                     perRecipeData.patient_name = e.name;
                     perRecipeData.email = e.email;
          
                   }
                 });
                 this.pharmacistData.forEach(e => {
                   if(r.pharmacist_id == e.id){
                    perRecipeData.pharmacist_name = e.name;
                   }
                 });
                 this.recipeSvc.addRecipe(perRecipeData);
               });

         },
         err => {
           console.log("Oops!");
         }
       )
   }
    


    signOut(){
      this.clearData();
      this.authSvc.logOut();
      this.nav.setRoot(this.signInPage);
      this.menuCtrl.close();
      console.log("logout")
  
    }
  
    clearData(){
      this.patientsData = [];
      this.doctorData = [];
      this.pharmacistData = [];
      this.recipeData = [];
      this.medinRecipeListData = [];

      this.patientSvc.clearDataList();
      this.doctorSvc.clearDataList();
      this.pharmacistSvc.clearDataList();
      this.recipeSvc.clearDataList();
      this.medListSvc.clearDataList();
      
    }
  
    onLoad(page: any){
      this.nav.setRoot(page);
      this.menuCtrl.close();
    }

  }