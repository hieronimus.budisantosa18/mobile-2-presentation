import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaftarPasienPage } from './daftar-pasien';

@NgModule({
  declarations: [
    DaftarPasienPage,
  ],
  imports: [
    IonicPageModule.forChild(DaftarPasienPage),
  ],
})
export class DaftarPasienPageModule {}
