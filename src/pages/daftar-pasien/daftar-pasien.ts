import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DaftarResepPage } from '../daftar-resep/daftar-resep';
import { ChattingPage } from "../chatting/chatting";
import { PatientService } from "../../services/patients";
import { DoctorService } from "../../services/doctors";
import { AuthService } from "../../services/auth";

import { Patient } from "../../data/patient.interface";
import { Doctor } from "../../data/doctor.interface";
/**
 * Generated class for the DaftarPasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daftar-pasien',
  templateUrl: 'daftar-pasien.html',
})
export class DaftarPasienPage {
  patientsData:Patient[] = [];
  doctorData: Doctor[] = [];
  doctorID:any;
  doctorName: any;
  patientID:any;
  accEmail:any;
  room:any;
  typeAccount: string;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private patientSvc: PatientService,
              private doctorSvc: DoctorService,
              private authSvc:AuthService,
             ) {
  }

  ngOnInit(): void {
    this.typeAccount = this.authSvc.getType();
    if(this.typeAccount == "dokter"){
      this.setDataDoctor();
    }else if(this.typeAccount == "pasien"){
      this.setDataPatient();
    }
  }

  setDataDoctor(){
    this.patientsData = this.patientSvc.getPatientList();
    this.doctorData = this.doctorSvc.getDoctorList();
    this.accEmail = this.authSvc.getUser().email
    console.log(this.accEmail);

    this.doctorData.forEach(r => {
      if(r.email == this.accEmail){
          this.doctorID = r.id;
          this.doctorName = r.name;
      }
    });
  }

  setDataPatient(){
    this.patientsData = this.patientSvc.getPatientList();
    this.doctorData = this.doctorSvc.getDoctorList();
    this.accEmail = this.authSvc.getUser().email
    console.log(this.accEmail);

    this.patientID = this.patientsData.find(x=>x.email == this.accEmail);
  }

  chattingDoctor(patient: Patient){
    this.room = this.doctorID + "-" + patient.id;
    this.navCtrl.push(ChattingPage, 
      {room: this.room, 
      target: patient.name,
      user: this.doctorName
    });
  }

  chattingPatient(doctor: Doctor){
    this.room = doctor.id + "-" + this.patientID.id;
    console.log(this.patientID);
    this.navCtrl.push(ChattingPage, 
      {room: this.room, 
      target: doctor.name,
      user: this.patientID.name
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DaftarPasienPage');
  }

}
