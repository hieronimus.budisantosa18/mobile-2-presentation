import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ToastController } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { ChattingService } from "../../services/chatting";
import { Chat } from "../../data/chat.interface";
import { AngularFireDatabase } from 'angularfire2/database-deprecated';


/**
 * Generated class for the ChattingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  
  selector: 'page-chatting',
  templateUrl: 'chatting.html',
})
export class ChattingPage {
  username: string = '';
  target:string ="";
  message: string = '';
  room: any;
  _chatSubscription;
  messages: object[] = [];
  constructor(private authSvc:AuthService, 
    public db: AngularFireDatabase, 
    public navCtrl: NavController, 
    public navParams: NavParams) {

    this.room = this.navParams.get('room');
    this.username = this.navParams.get('user');
    this.target = this.navParams.get('target');

    this._chatSubscription = this.db.list('/chat/' + this.room).subscribe( data => {
      this.messages = data;
    });
  }

  sendMessage() {
    
    this.db.list('/chat/'+this.room).push({
      username: this.username,
      message: this.message
    }).then( () => {
      // message is sent
    })
    this.message = '';

  }

  ngOnInit(): void {
    this.room = this.navParams.get('room');
    this.username = this.navParams.get('user');
    this.target = this.navParams.get('target');
    console.log(this.room);
  }

  ionViewDidLoad() {
 /*   this.db.list('/chat').push({
      specialMessage: true,
      message: `${this.username} has joined the room`
    });*/
  }

  ionViewWillLeave(){
    this._chatSubscription.unsubscribe();
  /*  this.db.list('/chat').push({
      specialMessage: true,
      message: `${this.username} has left the room`
    });*/

  }

}
