import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormIsiObatPage } from './form-isi-obat';

@NgModule({
  declarations: [
    FormIsiObatPage,
  ],
  imports: [
    IonicPageModule.forChild(FormIsiObatPage),
  ],
})
export class FormIsiObatPageModule {}
