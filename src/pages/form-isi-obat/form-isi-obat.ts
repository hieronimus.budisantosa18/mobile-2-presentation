import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DaftarResepPage } from '../daftar-resep/daftar-resep';

import { CategoryService } from "../../services/categories";
import { MedicineService } from "../../services/medicines";
import { DoseService } from "../../services/doses";
import { Category } from '../../data/category.interface';
import { Medicine } from '../../data/medicine.interface';
import { Dose } from '../../data/dose.interface';
import { NewRecipe } from '../../data/newRecipe.interface';
import { CreateRecipeService } from '../../services/createRecipe';
/**
 * Generated class for the FormIsiObatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-isi-obat',
  templateUrl: 'form-isi-obat.html',
})
export class FormIsiObatPage {
  private medicinesData : Medicine [] = [];
  private categoryData : Category [] = [];
  private doseData : Dose [] = [];
  private currentRecipe : NewRecipe;
  private doseForPatient : string;

  constructor(public navCtrl: NavController,
      public navParams: NavParams,
      private categorySvc :CategoryService,
      private medicineSvc :MedicineService,
      private doseeSvc :DoseService,
      public loadingCtrl: LoadingController,
      private createRecipeSvc: CreateRecipeService) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad FormIsiObatPage');
    this.currentRecipe = this.navParams.data;
    console.log(this.currentRecipe);
  }
  
  onButtonClicked() {
    this.navCtrl.push(DaftarResepPage);
  }

  obatForm: FormGroup;

  ngOnInit(){
    this.initializeForm();
    this.setData();
    this.currentRecipe = this.navParams.data;
  }

  setData(){
    console.log("SetData");
    this.medicinesData = this.medicineSvc.getDataList();
    this.categoryData = this.categorySvc.getDataList();
    this.doseData = this.doseeSvc.getDataList();
  }

  private initializeForm(){  
    
    this.obatForm = new FormGroup({
        name: new FormControl(null, Validators.required),
        doses: new FormControl(null, Validators.required),
        quantity: new FormControl(null, Validators.required),
        description: new FormControl(null, Validators.required)
        
      })
  }

  onSubmit(){
    var medicine = this.medicinesData.find(x=>x.id==this.obatForm.value.name);
    this.createRecipeSvc.addMedinRecipe(this.obatForm.value,medicine);
    console.log(this.createRecipeSvc.getMedinRecipe());
    //this.navCtrl.pop();
  }

  onSelectChange(selectedMedicine:any){
    console.log(selectedMedicine);
    var loading = this.loadingCtrl.create(
      {content:'Please Wait'}
    );

    loading.present();
    
    var catId;
    var medicine = this.medicinesData.find(x=>x.id==selectedMedicine);
    var category = this.categoryData.find(x=>x.id==medicine.category_id);
    
    this.doseData.forEach(s=>{
      if((category.id == s.category_id) 
      && this.currentRecipe.patient_age >= s.min_age 
      && this.currentRecipe.patient_age <= s.max_age){
        if(s.time_2> 0){
          this.doseForPatient = s.min_dose + " " + s.unit + " - " +
          s.max_dose + " " + s.unit + ", setiap " + s.time_1 + " " 
          + s.time_unit_1 + " atau " + s.time_2 + " " + s.time_unit_2;
        }else{
          this.doseForPatient = s.min_dose + " " + s.unit + " - " +
          s.max_dose + " " + s.unit + ", setiap " + s.time_1 + " " 
          + s.time_unit_1;
        }
      }
    })


    
    loading.dismiss();
    //this.doseForPatient  = "4 kali sehari";
    this.obatForm.patchValue({doses: this.doseForPatient});
    this.obatForm.patchValue({description: category.usage});
  }
}
