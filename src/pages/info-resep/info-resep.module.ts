import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfoResepPage } from './info-resep';

@NgModule({
  declarations: [
    InfoResepPage,
  ],
  imports: [
    IonicPageModule.forChild(InfoResepPage),
  ],
})
export class InfoResepPageModule {}
