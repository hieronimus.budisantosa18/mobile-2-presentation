import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { DaftarPasienPage } from "../daftar-pasien/daftar-pasien";
import { DaftarResepPage } from "../daftar-resep/daftar-resep";
import { NamaPasienPage } from "../nama-pasien/nama-pasien";
/**
 * Generated class for the BiodataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-biodata',
  templateUrl: 'biodata.html',
})
export class BiodataPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BiodataPage');
  }

}
