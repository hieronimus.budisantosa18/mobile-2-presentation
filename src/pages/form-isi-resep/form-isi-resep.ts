import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { DaftarResepPage } from "../daftar-resep/daftar-resep";
import { InfoResepPage } from "../info-resep/info-resep";
import { FormIsiObatPage } from "../form-isi-obat/form-isi-obat";
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { PatientService } from "../../services/patients";
import { PharmacistService } from "../../services/pharmacists";
import { CreateRecipeService } from "../../services/createRecipe";

import { Patient } from "../../data/patient.interface";
import { Pharmacist } from "../../data/pharmacist.interface";
/**
 * Generated class for the FormIsiResepPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-isi-resep',
  templateUrl: 'form-isi-resep.html',
})
export class FormIsiResepPage implements OnInit {
  patientsData: Patient[] = [];
  pharmacistsData: Pharmacist[] = [];
  resepForm : FormGroup;
  formIsiObatPage : FormIsiObatPage;
  patientAge : number;
  currentDate : Date;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private patientSvc: PatientService,
    private pharmacistSvc: PharmacistService,
    private createRecipeSvc: CreateRecipeService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormIsiResepPage');
  }

  onButtonClicked(){
    this.navCtrl.push(FormIsiObatPage);
  }
 

  ngOnInit(){
    this.setData();
    this.initializeForm();
  }


  setData(){
    console.log("SetData");
    this.patientsData = this.patientSvc.getPatientList();
    this.pharmacistsData = this.pharmacistSvc.getPharmacistList();
  }

  initializeForm(){
    var date = new Date().getDate;
    this.resepForm= new FormGroup({
      created_at: new FormControl(null, Validators.required),
      pharmacist_id: new FormControl("-- Pilih Apoteker --", Validators.required),
      patient_id: new FormControl("-- Pilih Pasien --", Validators.required),
      patient_age: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required)
    })
  }

  onSubmit(): void{
    this.createRecipeSvc.createRecipe(this.resepForm.value);
    this.navCtrl.push(FormIsiObatPage,this.resepForm.value);
    console.log(this.resepForm.value);
  }

  onSelectChange(selectedPatient:any){
    this.patientsData.forEach(r => {
      if(selectedPatient == r.id){
        var nowYear = new Date().getFullYear();
        var birthDate = r.birth_date;
        var birthYear = Number(birthDate.substr(0,4));


        this.patientAge = nowYear -  birthYear;
      }
    });

    this.resepForm.patchValue({patient_age: this.patientAge});
    
  }
}
