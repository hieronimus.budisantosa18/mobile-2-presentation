import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormIsiResepPage } from './form-isi-resep';

@NgModule({
  declarations: [
    FormIsiResepPage,
  ],
  imports: [
    IonicPageModule.forChild(FormIsiResepPage),
  ],
})
export class FormIsiResepPageModule {}
