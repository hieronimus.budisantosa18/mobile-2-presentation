import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalEditAlarmPage } from './modal-edit-alarm';

@NgModule({
  declarations: [
    ModalEditAlarmPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalEditAlarmPage),
  ],
})
export class ModalEditAlarmPageModule {}
