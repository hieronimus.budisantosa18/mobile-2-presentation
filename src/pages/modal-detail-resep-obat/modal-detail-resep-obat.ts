import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalDetailResepObatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-detail-resep-obat',
  templateUrl: 'modal-detail-resep-obat.html',
})
export class ModalDetailResepObatPage {
  medicine : any = [];
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController) {
    this.medicine = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuotePage');
  }


  closeModal(){
    this.viewCtrl.dismiss();
  }

}
