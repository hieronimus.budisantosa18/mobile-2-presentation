import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalDetailResepObatPage } from './modal-detail-resep-obat';

@NgModule({
  declarations: [
    ModalDetailResepObatPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalDetailResepObatPage),
  ],
})
export class ModalDetailResepObatPageModule {}
