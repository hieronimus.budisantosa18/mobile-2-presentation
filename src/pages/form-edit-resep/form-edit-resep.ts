import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { DaftarResepPage } from "../daftar-resep/daftar-resep";
import { InfoResepPage } from "../info-resep/info-resep";
import { FormIsiObatPage } from "../form-isi-obat/form-isi-obat";
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { PatientService } from "../../services/patients";
import { PharmacistService } from "../../services/pharmacists";
import { CreateRecipeService } from "../../services/createRecipe";

import { Patient } from "../../data/patient.interface";
import { Pharmacist } from "../../data/pharmacist.interface";

/**
 * Generated class for the FormEditResepPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-edit-resep',
  templateUrl: 'form-edit-resep.html',
})
export class FormEditResepPage implements OnInit {

  patientsData: Patient[] = [];
  pharmacistsData: Pharmacist[] = [];
  resepForm : FormGroup;
  formIsiObatPage : FormIsiObatPage;
  patientAge : number;
  currentDate : Date;
  currentRecipe : any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private patientSvc: PatientService,
    private pharmacistSvc: PharmacistService,
    private createRecipeSvc: CreateRecipeService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormIsiResepPage');
  }

  onButtonClicked(){
    this.navCtrl.push(FormIsiObatPage);
  }
 

  ngOnInit(){
    this.setData();
    this.initializeForm();
    this.currentRecipe = this.navParams.data;
    this.resepForm.patchValue({created_at: this.currentRecipe.created_at});
    this.resepForm.patchValue({pharmacist_id: this.currentRecipe.pharmacist_id});
    this.resepForm.patchValue({patient_id: this.currentRecipe.patient_id});
    this.resepForm.patchValue({patient_age: this.currentRecipe.patient_age});
    this.resepForm.patchValue({description: this.currentRecipe.description});
  }


  setData(){
    console.log("SetData");
    this.patientsData = this.patientSvc.getPatientList();
    this.pharmacistsData = this.pharmacistSvc.getPharmacistList();
  }

  initializeForm(){
    var date = new Date().getDate;
    this.resepForm= new FormGroup({
      created_at: new FormControl(null, Validators.required),
      pharmacist_id: new FormControl("-- Pilih Apoteker --", Validators.required),
      patient_id: new FormControl("-- Pilih Pasien --", Validators.required),
      patient_age: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required)
    })
  }

  cancel(){
    this.navCtrl.push(DaftarResepPage);
  }
  onSubmit(): void{
    this.createRecipeSvc.deleteRecipe();
    this.createRecipeSvc.createRecipe(this.resepForm.value);
    //console.log(this.createRecipeSvc.getMedinRecipe());
    this.navCtrl.push(DaftarResepPage); 
  }

  onSelectChange(selectedPatient:any){
    this.patientsData.forEach(r => {
      if(selectedPatient == r.id){
        var nowYear = new Date().getFullYear();
        var birthDate = r.birth_date;
        var birthYear = Number(birthDate.substr(0,4));


        this.patientAge = nowYear -  birthYear;
      }
    });

    this.resepForm.patchValue({patient_age: this.patientAge});
    
  }

}
