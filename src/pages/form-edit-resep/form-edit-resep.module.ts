import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormEditResepPage } from './form-edit-resep';

@NgModule({
  declarations: [
    FormEditResepPage,
  ],
  imports: [
    IonicPageModule.forChild(FormEditResepPage),
  ],
})
export class FormEditResepPageModule {}
