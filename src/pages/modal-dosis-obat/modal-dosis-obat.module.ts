import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalDosisObatPage } from './modal-dosis-obat';

@NgModule({
  declarations: [
    ModalDosisObatPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalDosisObatPage),
  ],
})
export class ModalDosisObatPageModule {}
