import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { DoseService } from "../../services/doses";

import { Dose } from "../../data/dose.interface";
/**
 * Generated class for the ModalDosisObatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-dosis-obat',
  templateUrl: 'modal-dosis-obat.html',
})
export class ModalDosisObatPage {
  private doseData : Dose [] = [];
  category : any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private doseSvc : DoseService) {
    this.category = this.navParams.data;
  }

  setData(){
    console.log("SetData");
    this.doseData = this.doseSvc.getDataList();
  }

  ionViewDidEnter() {
    this.setData();
    console.log('ionViewDidLoad QuotePage');
  }


  closeModal(){
    this.viewCtrl.dismiss();
  }

}
