import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllAlarmPage } from './all-alarm';

@NgModule({
  declarations: [
    AllAlarmPage,
  ],
  imports: [
    IonicPageModule.forChild(AllAlarmPage),
  ],
})
export class AllAlarmPageModule {}
