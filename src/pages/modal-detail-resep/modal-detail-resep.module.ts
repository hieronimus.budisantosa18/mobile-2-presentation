import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalDetailResepPage } from './modal-detail-resep';

@NgModule({
  declarations: [
    ModalDetailResepPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalDetailResepPage),
  ],
})
export class ModalDetailResepPageModule {}
