import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalDetailResepPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-detail-resep',
  templateUrl: 'modal-detail-resep.html',
})
export class ModalDetailResepPage {

  recipe : any = [];
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController) {
    this.recipe = this.navParams.data;
    console.log(this.recipe);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuotePage');
  }


  closeModal(){
    this.viewCtrl.dismiss();
  }
}
