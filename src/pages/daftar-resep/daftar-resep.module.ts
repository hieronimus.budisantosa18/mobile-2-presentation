import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaftarResepPage } from './daftar-resep';

@NgModule({
  declarations: [
    DaftarResepPage,
  ],
  imports: [
    IonicPageModule.forChild(DaftarResepPage),
  ],
})
export class DaftarResepPageModule {}
