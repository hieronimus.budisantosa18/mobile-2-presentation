import { Component, OnInit, ViewChild, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, ViewController } from 'ionic-angular';

import { TabsPage } from "../tabs/tabs";
import { DaftarPasienPage } from "../daftar-pasien/daftar-pasien";
import { FormIsiResepPage } from "../form-isi-resep/form-isi-resep";
import { FormIsiObatPage } from "../form-isi-obat/form-isi-obat";
import { ModalDetailResepObatPage } from "../modal-detail-resep-obat/modal-detail-resep-obat";
import { ModalDetailResepPage } from "../modal-detail-resep/modal-detail-resep";

import { PatientService } from "../../services/patients";
import { DoctorService } from "../../services/doctors";
import { AuthService } from "../../services/auth";
import { RecipeService } from "../../services/recipes";
import { MedicineListService } from "../../services/medicinelists";
import { PharmacistService } from "../../services/pharmacists";
import { CreateRecipeService } from "../../services/createRecipe";

import { Recipe } from "../../data/recipe.interface";
import { RecipeList } from "../../data/recipeList.interface";
import { Patient } from "../../data/patient.interface";
import { Doctor } from "../../data/doctor.interface";
import { Account } from "../../data/Account.interface";
import { NewRecipe } from '../../data/newRecipe.interface';
import { FormEditObatPage } from '../form-edit-obat/form-edit-obat';
import { FormEditResepPage } from '../form-edit-resep/form-edit-resep';
import { MedicineList } from '../../data/medicinelist.interface';
import { Pharmacist } from '../../data/pharmacist.interface';
import { CategoryService } from '../../services/categories';
import { Category } from '../../data/category.interface';
import { AlarmPage } from '../alarm/alarm';
import 'rxjs/add/operator/map'
/**
 * Generated class for the DaftarResepPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daftar-resep',
  templateUrl: 'daftar-resep.html',
})
export class DaftarResepPage implements OnInit {
  recipesData: Recipe[] = [];
  recipesListData: any[] = [];
  patientsData: Patient[] = [];
  doctorData: Doctor[] = [];
  pharmacistData: Pharmacist[] = [];
  categoryListData: Category[] = [];
  medListData :any [] = [];
  accEmail:any;
  activeAccount:any;
  recipeList: RecipeList;
  recipeStatus : boolean;
  recentRecipe : any;
  loading: any = this.loadingCtrl;
  modalDetailResepObat = ModalDetailResepObatPage;
  modalDetailResep = ModalDetailResepPage;
  formIsiResepPage = FormIsiResepPage;
  newMedinRecipeList: any []=[];
  newRecipePatient : Patient;

  recipeReady: Recipe;
  medInRecipeReady : MedicineList [] = [];

  typeAccount: string;
  shownGrup = null;

  public buttonClicked: boolean = false;
  public buttonClicked2: boolean = true;

  ///////////////////////////////////////////////
  accordionExapanded = false;
  @ViewChild("nenen") cardContent: any;
  ///////////////////////////////////////////////

  shownGroup = null;

  
  ///////////////////////////////////////////////

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private patientSvc: PatientService,
    private doctorSvc: DoctorService,
    private authSvc:AuthService,
    private recipeSvc:RecipeService,
    private loadingCtrl: LoadingController,
    private medListSvc:MedicineListService,
    private modalCtrl: ModalController,
    private viewCtrl: ViewController,
    private pharmacistSvc : PharmacistService,
    private categorySvc : CategoryService,
    private createRecipeSvc: CreateRecipeService,
    public renderer: Renderer ) {
      //this.setData();
      this.typeAccount = this.authSvc.getType();
  }

  ngOnInit(): void {
    
    

    this.recipeStatus = this.createRecipeSvc.getStatus();

    this.typeAccount = this.authSvc.getType();
    console.log(this.typeAccount);
    if(this.typeAccount == "dokter"){
      this.setDataDoctor();
    }else if(this.typeAccount == "pasien"){
      this.setDataPatient();
    }

    
  }


  toggleAccordion(){
		if(this.accordionExapanded){
			this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
		} else {
			this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "500px");
		}
		this.accordionExapanded = !this.accordionExapanded;
	}

  setDataDoctor(){
    console.log("SetDocter");
    this.patientsData = this.patientSvc.getPatientList();
    this.pharmacistData = this.pharmacistSvc.getPharmacistList();
    this.doctorData =  this.doctorSvc.getDoctorList();
    this.recipesData =  this.recipeSvc.getRecipeList();
    this.medListData =  this.medListSvc.getMedlist();
    this.categoryListData =  this.categorySvc.getDataList();
    this.accEmail = this.authSvc.getUser().email;
    console.log(this.accEmail);
    this.doctorData.forEach(r => {
      this.doctorSvc.addDoctor(r);
      if(r.email == this.accEmail){
          this.authSvc.setActiveAccount(r);
      }
    });
    this.activeAccount = this.doctorData.find(x=>x.email == this.accEmail);
  }

  setDataPatient(){
    console.log("SetPatient");
    this.patientsData = this.patientSvc.getPatientList();
    this.pharmacistData = this.pharmacistSvc.getPharmacistList();
    this.doctorData =  this.doctorSvc.getDoctorList();
    this.recipesData =  this.recipeSvc.getRecipeList();
    this.medListData =  this.medListSvc.getMedlist();
    this.categoryListData =  this.categorySvc.getDataList();
    this.accEmail = this.authSvc.getUser().email;
    console.log(this.recipesData);
    console.log(this.accEmail);
    this.activeAccount = this.doctorData.find(x=>x.email == this.accEmail);
  }

  ionViewDidEnter() {
    this.typeAccount = this.authSvc.getType();
    console.log('ionViewDidLoad DaftarObatPage');
    this.recipeStatus = this.createRecipeSvc.getStatus();
    if(this.createRecipeSvc.getStatus != null){
      this.recentRecipe = this.createRecipeSvc.getRecipe();
      try{
        this.newRecipePatient = this.patientsData.find(x=>x.id==this.recentRecipe.patient_id);
      }catch(err){
        this.newRecipePatient = null;
      }
      console.log(this.newRecipePatient);
      //this.newRecipePatient = patient.name;
    }
    this.newMedinRecipeList = this.createRecipeSvc.getMedinRecipe();
  }

  detailObatResep(data){
    let modal = this.modalCtrl.create(this.modalDetailResepObat,data);
    modal.onDidDismiss(data=> {

    });
    modal.present();
  }

  detailResep(data){
    let modal = this.modalCtrl.create(this.modalDetailResep,data);
    modal.onDidDismiss(data=> {

    });
    modal.present();
  }

  buatResepBaru(){
    this.navCtrl.push(this.formIsiResepPage);
  }

  tambahObat(){
    this.navCtrl.push(FormIsiObatPage,this.recentRecipe);
  }

  deleteMed(m){
    this.createRecipeSvc.deleteMedinRecipe(m);
    this.newMedinRecipeList = this.createRecipeSvc.getMedinRecipe();
  }

  editMed(m){
    this.navCtrl.push(FormEditObatPage,m);
  }

  editRecipe(r){
    this.navCtrl.push(FormEditResepPage,r);
  }
  deleteRecipe(){
    this.createRecipeSvc.deleteRecipe();
    this.recentRecipe = null;
  }
   
  konfirmasiResep(){
    this.loading = this.loadingCtrl.create(
      {content:'Please Wait'}
    );
    this.loading.present();
    this.recipesData = this.recipeSvc.getRecipeList();
    this.recipeReady = this.recentRecipe;
    this.activeAccount = this.doctorData.find(x=>x.email == this.accEmail);
    var pharmacist = this.pharmacistData.find(x => x.id == this.recipeReady.pharmacist_id);
    var patient = this.patientsData.find(x => x.id == this.recipeReady.patient_id);
    this.recipeReady.doctor_id = this.activeAccount.id;
    this.recipeReady.doctor_name = this.activeAccount.name;
    this.recipeReady.email = this.activeAccount.email;

    this.recipeReady.patient_name = patient.name;
    this.recipeReady.id = this.recipesData[this.recipesData.length-1].id + 1;

    this.recipeSvc.addRecipe(this.recipeReady);
    this.recipesData = this.recipeSvc.getRecipeList();

    
    console.log(this.recipesData);
    //console.log(this.newMedinRecipeList);
    this.newMedinRecipeList.forEach(x=>{
      this.medInRecipeReady.push(x);
    });
    this.medInRecipeReady.forEach(x=>{
      x.id = this.medListData[this.medListData.length-1].id + 1;
      x.recipe_id = this.recipeReady.id;
      x.medicine_id = this.medListData.find(y=>y.name == x.name).id;
      x.status = this.categoryListData.find(y=>y.id == x.medicine_id).info;
      this.medListData.push(x);
    });

    //console.log(this.newMedinRecipeList);
    console.log(this.medListData);

    this.recipeSvc.create(this.recipeReady);
    this.medInRecipeReady.forEach(x=>{
      this.medListSvc.create(x);
    })

    this.createRecipeSvc.setStatus(false);
    this.createRecipeSvc.deleteRecipe();
    this.createRecipeSvc.deleteAllMedRecipe();
    this.recipeStatus = this.createRecipeSvc.getStatus();
    this.newMedinRecipeList = this.createRecipeSvc.getMedinRecipe();
    this.recentRecipe = this.createRecipeSvc.getRecipe();
    this.medInRecipeReady = [];
    this.loading.dismiss();
    //this.recipeSvc.create(this.recipeReady);
  }

  btnShowClicked(){
    this.buttonClicked = !this.buttonClicked;
    this.buttonClicked2 = !this.buttonClicked2;
  }

  btnShowClicked2(){
    this.buttonClicked = !this.buttonClicked;
    this.buttonClicked2 = !this.buttonClicked2;
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  };
  isGroupShown(group) {
      return this.shownGroup === group;
  };

  setAlarm(medicine){
    this.navCtrl.push(AlarmPage,medicine);
  }


  //UI Logic end
}
