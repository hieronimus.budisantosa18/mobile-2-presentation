import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DaftarResepPage } from "../daftar-resep/daftar-resep";
import { InfoResepPage } from "../info-resep/info-resep";
import { DaftarPasienPage } from "../daftar-pasien/daftar-pasien";
import { DaftarObatPage } from "../daftar-obat/daftar-obat";
import { UserAccount } from '../../data/userAccount.interface';
import { AuthService } from '../../services/auth';
import { PatientService } from '../../services/patients';
import { DoctorService } from '../../services/doctors';
import { Patient } from '../../data/patient.interface';
import { Doctor } from '../../data/doctor.interface';
/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage implements OnInit  {
  userAccount : UserAccount;
  typeAccount : string;

  daftarResepPage = DaftarResepPage;
  infoResepPage = InfoResepPage;
  daftarPasienPage = DaftarPasienPage;
  daftarObatPage = DaftarObatPage;

  patientsData: Patient[] = [];
  doctorData: Doctor[] = [];
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private authSvc : AuthService,
    private patientSvc: PatientService,
    private doctorSvc: DoctorService,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

  ngOnInit(): void {
    this.typeAccount = this.navParams.data;
    this.authSvc.setType(this.typeAccount);
  }

}
