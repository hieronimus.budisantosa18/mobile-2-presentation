import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TabsPage } from "../tabs/tabs";
import { AuthService } from "../../services/auth";
import { UserService } from '../../services/users';
import firebase from 'firebase';

/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage implements OnInit {
  userForm: FormGroup;
  pass: any;
  email : any;
  user: any;
  type : any;
  allUser: any
  arr: any = [];

  constructor(private usersSvc:UserService, private events:Events, private authSvc:AuthService, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }

  ngOnInit(): void {
    this.intializeForm();
  }

  private intializeForm(){
    this.userForm = new FormGroup({
      email : new FormControl(null, Validators.required),
      password : new FormControl(null, Validators.required),
      type : new FormControl(null, Validators.required)
    })
  }
  
  onSubmit(){
    this.email = this.userForm.value.email;
    this.pass = this.userForm.value.password; 

    console.log(this.type);
    this.authSvc.signIn(this.email, this.pass)
      .then((firebaseUser)=>{
        //this.user = firebaseUser.email;
        //this.authSvc.setSession(true);
        //this.authSvc.setUser(this.user);
        
         this.usersSvc.getAllUser().subscribe(
          data=>{
            this.allUser = data.data;
            console.log(data.data);
            console.log(this.allUser);
            this.authSvc.setType(this.type);
            //console.log(this.allUser[1].email);

           // this.type = this.userForm.value.type;
           // this.usersSvc.setUserData(this.allUser);

            //console.log(this.usersSvc.getAllUser());
            
          },
          err => {
            console.log("Oops!");
          }
        );
        //console.log(this.allUser);

        /*
        for(var k in this.allUser){
          if(this.email == k['email']){
            this.navCtrl.push(TabsPage);
            console.log(k['email']);
            break;
          }
        }
        for(var u in this.allUser){
          this.arr.push(u);
          console.log(this.arr);
        }*/
        this.usersSvc.getUserData();

      });
   
      /*
      .catch(function(error){
        console.log(error.message);
      });*/
    
  }

}
