import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { Medicine } from "../../data/medicine.interface";
import { Category } from "../../data/category.interface";
import { Dose } from "../../data/dose.interface";

import { CategoryService } from "../../services/categories";
import { MedicineService } from "../../services/medicines";
import { DoseService } from "../../services/doses";

import { ModalDosisObatPage } from "../modal-dosis-obat/modal-dosis-obat";
/**
 * Generated class for the DaftarObatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daftar-obat',
  templateUrl: 'daftar-obat.html',
})
export class DaftarObatPage {
  private medicinesData : Medicine [] = [];
  private categoryData : Category [] = [];

  public buttonClicked: boolean = false;
  public buttonClicked2: boolean = true;

  modalDetailResepObat = ModalDosisObatPage;

  shownGroup = null;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private categorySvc: CategoryService,
    private MedicineSvc: MedicineService,
    private DoseSvc: DoseService,
    private modalCtrl: ModalController,) {
      this.setData();
  }

  ngOnInit(): void {
    this.setData();
  }

  setData(){
    console.log("SetData");
    this.medicinesData = this.MedicineSvc.getDataList();
    this.categoryData =  this.categorySvc.getDataList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DaftarObatPage');
  }

  btnShowClicked(){
    this.buttonClicked = !this.buttonClicked;
    this.buttonClicked2 = !this.buttonClicked2;
  }

  btnShowClicked2(){
    this.buttonClicked = !this.buttonClicked;
    this.buttonClicked2 = !this.buttonClicked2;
  }

  dosis(data){
    let modal = this.modalCtrl.create(this.modalDetailResepObat,data);
    modal.onDidDismiss(data=> {

    });
    modal.present();
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  };
  isGroupShown(group) {
      return this.shownGroup === group;
  };
}
