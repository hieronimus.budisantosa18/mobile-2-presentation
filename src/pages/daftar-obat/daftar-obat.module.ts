import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaftarObatPage } from './daftar-obat';

@NgModule({
  declarations: [
    DaftarObatPage,
  ],
  imports: [
    IonicPageModule.forChild(DaftarObatPage),
  ],
})
export class DaftarObatPageModule {}
