import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { DaftarResepPage } from "../daftar-resep/daftar-resep";
import { InfoResepPage } from "../info-resep/info-resep";
import { DaftarPasienPage } from "../daftar-pasien/daftar-pasien";
/**
 * Generated class for the NamaPasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nama-pasien',
  templateUrl: 'nama-pasien.html',
})
export class NamaPasienPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NamaPasienPage');
  }

}
