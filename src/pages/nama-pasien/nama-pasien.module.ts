import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NamaPasienPage } from './nama-pasien';

@NgModule({
  declarations: [
    NamaPasienPage,
  ],
  imports: [
    IonicPageModule.forChild(NamaPasienPage),
  ],
})
export class NamaPasienPageModule {}
