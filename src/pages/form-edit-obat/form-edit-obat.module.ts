import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormEditObatPage } from './form-edit-obat';

@NgModule({
  declarations: [
    FormEditObatPage,
  ],
  imports: [
    IonicPageModule.forChild(FormEditObatPage),
  ],
})
export class FormEditObatPageModule {}
