import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, ModalController } from 'ionic-angular';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Medicine } from '../../data/medicine.interface';
import { Alarm } from '../../data/alarm.interface';
import { ModalSetAlarmPage } from "../modal-set-alarm/modal-set-alarm";
import { ModalEditAlarmPage } from '../modal-edit-alarm/modal-edit-alarm';
import { MedicineList } from '../../data/medicinelist.interface';
import { MedicineListService } from '../../services/medicinelists';
/**
 * Generated class for the AlarmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alarm',
  templateUrl: 'alarm.html',
})
export class AlarmPage implements OnInit {
  alarm:Alarm;
  alarmList:any[]=[];
  medicine:MedicineList;
  maxAlarm:boolean;
  medListData :any [] = [];
  multiNotif:any [] = [];
  constructor(public navCtrl: NavController, 
    private platform: Platform, 
    private localNotif: LocalNotifications, 
    private alertCtrl: AlertController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private medListSvc:MedicineListService,
    
    
  ) {
    this.medicine = this.navParams.data;
    this.platform.ready().then((readySource) => {
      
      this.localNotif.on('click', (notification, state) => {
        let json = JSON.parse(notification.data);
        
        let alert = alertCtrl.create({
          title: notification.title,
          subTitle: json.mydata
        });
        alert.present();
        //this.nativeAudio.stop('uniqueId1');
      })
    });
  }


  ngOnInit(): void {
    
    this.setData();
    
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad AlarmPage');
    this.checkMax();
  }

  setData(){
    this.medicine = this.navParams.data;
    this.medListData =  this.medListSvc.getMedlist();
    if (this.medicine.alarm_1 != null) {
      this.alarmList.push(this.medicine.alarm_1);
    }
    if (this.medicine.alarm_2 != null) {
      this.alarmList.push(this.medicine.alarm_2);
    }
    if (this.medicine.alarm_3 != null) {
      this.alarmList.push(this.medicine.alarm_3);
    }
    console.log(this.medicine);
  }

  checkMax(){
    if(this.alarmList.length > 2){
      this.maxAlarm = true;
    }else{
      this.maxAlarm = false;
    }
  }

  tambahAlarm(){
    let modal = this.modalCtrl.create(ModalSetAlarmPage);
    modal.onDidDismiss(data=> {
      if(data!=undefined){
        this.alarmList.push(data);
        this.medicine.alarm_1 = this.alarmList[0];
        this.medicine.alarm_2 = this.alarmList[1];
        this.medicine.alarm_3 = this.alarmList[2];
        this.medListSvc.update(this.medicine);
        this.checkMax();
        this.scheduleNotification();
        //this.setData();
      }
    });
    modal.present();
  }

  editAlarm(x){
    this.alarmList
    .splice(this.alarmList.indexOf(x),1);
    console.log("edit");
    let modal = this.modalCtrl.create(ModalEditAlarmPage,x);
    modal.onDidDismiss(data=> {
      if(data!=undefined){
        this.alarmList.push(data);
        this.checkMax();
      }
    });
    modal.present();
  }

  deleteAlarm(x){
    this.alarmList
    .splice(this.alarmList.indexOf(x),1);
  }

  scheduleNotification() {
    if(this.alarmList[0] != null){
      this.localNotif.schedule({
        id: 1,
        title: 'Attention',
        text: this.alarmList[0] + ', Saatnya makan obat' + this.medicine.name,
        data: { mydata: 'Saatnya makan obat ' + this.medicine.name },
        at: new Date().setTime(this.alarmList[0]),
        sound: 'res://platform_default'
      });
    }
  }
}
