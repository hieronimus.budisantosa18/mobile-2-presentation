import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, ViewController } from 'ionic-angular';
import { Medicine } from '../../data/medicine.interface';
import { LocalNotifications } from '@ionic-native/local-notifications';

/**
 * Generated class for the ModalSetAlarmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-set-alarm',
  templateUrl: 'modal-set-alarm.html',
})
export class ModalSetAlarmPage {

  alertTime:any;
  medicine:Medicine;
  
  constructor(public navCtrl: NavController, 
    private platform: Platform, 
    private localNotif: LocalNotifications, 
    private alertCtrl: AlertController,
    public navParams: NavParams,
    private viewCtrl: ViewController,) {
    //this.alertTime = this.navParams.data;
  

  }

addAlarm(){
  console.log(this.alertTime);
  if(this.alertTime != undefined){
    this.viewCtrl.dismiss(this.alertTime);
  }else{
    //Toast karena waktu masih tidak disisi atau kosong
  }
}

scheduleNotification() {
  console.log(this.alertTime);

  this.localNotif.schedule({
    id: 1,
    title: 'Attention',
    text: this.alertTime,
    data: { mydata: 'My hidden message this is' },
    at: new Date().setTime(this.alertTime),
    sound: 'res://platform_default'
  });

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlarmPage');
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

}
