import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalSetAlarmPage } from './modal-set-alarm';

@NgModule({
  declarations: [
    ModalSetAlarmPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalSetAlarmPage),
  ],
})
export class ModalSetAlarmPageModule {}
