import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TambahPasienPage } from './tambah-pasien';

@NgModule({
  declarations: [
    TambahPasienPage,
  ],
  imports: [
    IonicPageModule.forChild(TambahPasienPage),
  ],
})
export class TambahPasienPageModule {}
