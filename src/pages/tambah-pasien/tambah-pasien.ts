import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { DaftarResepPage } from "../daftar-resep/daftar-resep";
import { InfoResepPage } from "../info-resep/info-resep";
import { DaftarPasienPage } from "../daftar-pasien/daftar-pasien";
/**
 * Generated class for the TambahPasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tambah-pasien',
  templateUrl: 'tambah-pasien.html',
})
export class TambahPasienPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TambahPasienPage');
  }

}
