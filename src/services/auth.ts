import firebase from 'firebase';
import { Injectable } from "@angular/core";
import { Account } from "../data/account.interface";
import { UserAccount } from '../data/userAccount.interface';



@Injectable()
export class AuthService{
    user:any = null;
    id:any = null;
    account: Account;
    userAccount : UserAccount;
    accountType: string;

    signUp(email : string, password: string){
        return firebase.auth().createUserWithEmailAndPassword(email,password);
    }

    signIn(email : string, password: string){
        return firebase.auth().signInWithEmailAndPassword(email,password);
    }

    logOut(){
        firebase.auth().signOut();
    }
    
    setUser(user){
        this.user = user;
    }
    

    getUser(){
        return this.user;
    }

    getActiveUser (){
        console.log(firebase.auth().currentUser);
        console.log(firebase.auth().currentUser.uid);
        return firebase.auth().currentUser;
    }

    setActiveAccount(acc :Account){
        this.account = acc;
    }

    getActiveAccount(){
        return this.account;
    }

    setUserAccount(userAccount: UserAccount){
        this.userAccount = userAccount;
    }

    getUserAccount(){
        return this.userAccount;
    }

    setType(val){
        this.accountType = val;
    }

    getType(){
        return this.accountType;
    }
}