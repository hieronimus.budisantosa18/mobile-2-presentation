import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { User } from "../data/user.interface";
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';

// @Pipe({ name: 'keys', pure: false})
@Injectable()
export class UserService {
    private url = 'http://10.0.0.101:8080/e_recipe_API/e-recipe-api/public/v1/users/';
    private data : any;
    private allUser : any;
    private allUser1 : any;
    constructor(
        private http: Http,
    ){}
    // transform(
    //     value: any = this.http.get(this.url + 'view-all').map(res =>res.json().data.description),
    //      args: any[] = null): any {
    //     return Object.keys(value).map(key => value[key]);
    // }

    getAllUser() {
        this.fetchData();
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }
    fetchData(){

        this.http.get(this.url + 'view-all').map(res => res.json()).subscribe(
            data=> {
              this.data =  data.data;
              console.log(this.data);
              //console.log(this.data[0]['email'])
            },
            err=> {
                console.log("error");
            }
        );
        if(typeof this.data !== 'undefined'){
            this.allUser = JSON.parse(this.data);
        }
    }
    getUserDetail(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }
    setUserData(data:any){
        this.allUser1 = data;
       // console.log(this.allUser1[0].last_name);
    }
    getUserData(){
        //console.log(this.allUser1[0].last_name);
       // return this.allUser1;
    }
}