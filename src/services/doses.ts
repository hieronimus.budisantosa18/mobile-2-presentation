import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';
import { Dose } from "../data/dose.interface";

@Injectable()
export class DoseService {
    private doseList: Dose[] = [];
    private url = 'http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/doses/';
    constructor(
        private http: Http,
    ){}

    read() {
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }

    view(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }

    create(data) {
        // console.log(data.genericName);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'min_dose=' + data.min_dose +
            '&max_dose=' + data.max_dose +
            '&unit=' + data.unit +
            '&min_age=' + data.min_age +
            '&max_age=' + data.max_age +
            '&time_1=' + data.time_1 +
            '&time_unit_1=' + data.time_unit_1 +
            '&time_2=' + data.time_2 +
            '&time_unit_2=' + data.time_unit_2 +
            '&category_id=' + data.category_id;

        return this.http.post(this.url + 'create', body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            );
    }

    update(data) {
        // console.log(data);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'min_dose=' + data.min_dose +
            '&max_dose=' + data.max_dose +
            '&unit=' + data.unit +
            '&min_age=' + data.min_age +
            '&max_age=' + data.max_age +
            '&time_1=' + data.time_1 +
            '&time_unit_1=' + data.time_unit_1 +
            '&time_2=' + data.time_2 +
            '&time_unit_2=' + data.time_unit_2 +
            '&category_id=' + data.category_id;

        return this.http.post(this.url + 'update/' + data.id , body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            ); 
    }

    delete(id) {
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.url + 'delete/' + id, '', options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            ); 
    }

    getDataList() {
        return this.doseList;
    }

    clearDataList(){
        this.doseList = [];
    }

    addDataList(dose:Dose){
        this.doseList.push(dose);
    }
}