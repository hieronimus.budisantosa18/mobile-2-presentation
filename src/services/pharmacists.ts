import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { Pharmacist } from "../data/pharmacist.interface";

@Injectable()
export class PharmacistService {
    private pharmacistList: Pharmacist[] = [];
    private url = 'http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/pharmacists/';
    constructor(
        private http: Http,
    ){}

    getAllPharmacist() {
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }

    getPharmacist(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }

    addPharmacist(pharmacist:Pharmacist){
        this.pharmacistList.push(pharmacist);
    }

    getPharmacistList() {
        return this.pharmacistList;
    }

    clearDataList(){
        this.pharmacistList = [];
    }

}