import { Injectable } from '@angular/core';
import { Alarm } from "../data/alarm.interface";
import { Http, Headers, RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class AlarmService {
    private url = 'http://localhost/e-recipe/public/v1/alarms/';
    constructor(
        private http: Http,
    ){}

    getAllAlarm() {
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }

    getAlarmDetail(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }

    createAlarm() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let datas = {
            time: '17:00:00',
            category: 'ronal',
        }
        return this.http
            .post(this.url + 'create', JSON.stringify(datas))
            // .map(res => res.json())
            .subscribe(
                data => {
                    console.log(data.json())
                }
            )
    }
}