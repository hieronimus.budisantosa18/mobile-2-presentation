import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';
import { Category } from "../data/category.interface";

@Injectable()
export class CategoryService {
    private categoryList: Category[] = [];
    //private url = 'http://10.0.2.2:8080/e_recipe_API/e-recipe-api/public/v1/categories/';
    //private url = 'http://10.0.0.101:8080/e_recipe_API/e-recipe-api/public/v1/categories/';    
    private url = 'http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/categories/';
    constructor(
        private http: Http,
    ){}

    read() {
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }

    view(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }

    create(data) {
        // console.log(data.genericName);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'name=' + data.name +
            '&usage=' + data.usage +
            '&prohibition=' + data.prohibition +
            '&info=' + data.info;

        return this.http.post(this.url + 'create', body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            );
    }

    update(data) {
        // console.log(data);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'name=' + data.name +
            '&usage=' + data.usage +
            '&prohibition=' + data.prohibition +
            '&info=' + data.info;

        return this.http.post(this.url + 'update/' + data.id , body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            ); 
    }

    delete(id) {
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.url + 'delete/' + id, '', options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            ); 
    }

    getDataList() {
        return this.categoryList;
    }

    clearDataList(){
        this.categoryList = [];
    }

    addDataList(cat:Category){
        this.categoryList.push(cat);
    }
}