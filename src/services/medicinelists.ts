import { Injectable } from '@angular/core';
import { Http,Headers, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import { MedicineList } from "../data/medicinelist.interface";

@Injectable()
export class MedicineListService {
    private medicineist: MedicineList[] = [];
    private url = 'http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/medicine-list/';
    
    constructor(
        private http: Http,
    ) {}

    getAllMedicineLists() {
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }

    getMedicineList(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }

    create(data: MedicineList) {
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'name=' + data.name +
            '&doses=' + data.doses +
            '&status=' + data.status +
            '&description=' + data.description +
            '&quantity=' + data.quantity +
            '&medicine_id=' + data.medicine_id +
            '&recipe_id=' + data.recipe_id +
            '&alarm_1=' + data.alarm_1 +
            '&alarm_2=' + data.alarm_2 +
            '&alarm_3=' + data.alarm_3 +
            '&alarm_desc_1=' + data.alarm_desc_1 +
            '&alarm_desc_2=' + data.alarm_desc_2 +
            '&alarm_desc_3=' + data.alarm_desc_3;

        return this.http.post(this.url + 'create', body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            );
    }

    update(data) {
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'alarm_1=' + data.alarm_1 +
            '&alarm_2=' + data.alarm_2 +
            '&alarm_3=' + data.	alarm_3;
        return this.http.post(this.url + 'update/' + data.id , body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            );
    }


    addMedlist(medicList:MedicineList){
        this.medicineist.push(medicList);
    }

    getMedlist() {
        return this.medicineist;
    }
    clearDataList(){
        this.medicineist = [];
    }
}