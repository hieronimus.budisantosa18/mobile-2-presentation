import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import { Medicine } from "../data/medicine.interface";

@Injectable()
export class MedicineService {
    private medicineList: Medicine[] = [];
    private url = 'http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/medicines/';
    
    constructor(
        private http: Http,
    ) {}

    read() {
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }

    view(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }

    create(data) {
        // console.log(data.genericName);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'name=' + data.name +
            '&generic_name=' + data.genericName +
            '&summary=' + data.summary +
            '&description=' + data.description;

        return this.http.post(this.url + 'create', body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            );
    }

    update(data) {
        // console.log(data);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'name=' + data.name +
            '&generic_name=' + data.genericName +
            '&summary=' + data.summary +
            '&description=' + data.description;

        return this.http.post(this.url + 'update/' + data.id , body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            ); 
    }

    delete(id) {
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.url + 'delete/' + id, '', options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            ); 
    }

    addDataList(med:Medicine){
        this.medicineList.push(med);
    }

    getDataList() {
        return this.medicineList;
    }

    clearDataList(){
        this.medicineList = [];
    }
}