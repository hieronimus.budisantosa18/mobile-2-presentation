import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { Patient } from "../data/patient.interface";

@Injectable()
export class PatientService {
    private patientList: Patient[] = [];
    private url = 'http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/patients/';
    constructor(
        private http: Http,
    ){}

    addPatient(patient:Patient){
        this.patientList.push(patient);
    }

    getAllPatients() {
    
         return this.http.get(this.url + 'view-all').map(res => res.json());
            
    }

    getPatient(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }

    getPatientList(){
        return this.patientList;
    }

    clearDataList(){
        this.patientList = [];
    }
}