import { Chat } from "../data/chat.interface";
import { AuthService } from "../services/auth";
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from "@angular/core";
import firebase from 'firebase';



@Injectable()
export class ChattingService{
    private messages: Chat[] = [];

    constructor(public authSvc: AuthService, public http: Http) {
    }

    addQuoteToFavorites(chat:Chat){

        this.messages.push(chat);
    }

    storeList(token: any, chat: any){

        var uid = this.authSvc.getActiveUser().uid;
        
        return this.http      
            .put('https://favoritequotesapp-73388.firebaseio.com/' + 
            '/chats.json?auth=' + token, this.messages)
            .map(response=>{
               return response.json();
               });
            
    }

    getMessages(token: any){
        var uid = this.authSvc.getActiveUser().uid;
        return this.http
            .get('https://favoritequotesapp-73388.firebaseio.com/'+ uid +
            '/chatting.json?auth=' + token);
    }


}