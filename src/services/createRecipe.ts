import { Chat } from "../data/chat.interface";
import { AuthService } from "../services/auth";
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from "@angular/core";
import firebase from 'firebase';
import { NewRecipe } from "../data/newRecipe.interface";
import { MedicineList } from "../data/medicinelist.interface";
import { Medicine } from "../data/medicine.interface";

@Injectable()
export class CreateRecipeService{
    private newRecipe: NewRecipe;
    private newMedicineinRecipe : MedicineList [] = [];
    private medList : MedicineList;
    private newRecipeStatus : boolean;
    

    constructor(public authSvc: AuthService, public http: Http) {
        this.newRecipeStatus  = false;
    }

    addMedinRecipe (medinRecipe,med:Medicine){
        this.medList = medinRecipe;
        this.medList.alarm_1 = 0;
        this.medList.alarm_desc_1 = "";
        this.medList.alarm_2 = 0;
        this.medList.alarm_desc_2 = "";
        this.medList.alarm_3 = 0;
        this.medList.alarm_desc_3 = "";
        this.medList.name = med.name;
        this.newMedicineinRecipe.push(this.medList);
    }

    createRecipe(recipe){
        this.newRecipe = recipe;
        this.newRecipeStatus = true;
    }

    setStatus(f:boolean){
         this.newRecipeStatus = f;
    }


    getRecipe(){
        return this.newRecipe;
    }

    getStatus(){
        return this.newRecipeStatus;
    }

    getMedinRecipe(){
        return this.newMedicineinRecipe;
    }

    deleteMedinRecipe(m){
         this.newMedicineinRecipe
            .splice(this.newMedicineinRecipe.indexOf(m),1);
    }
    deleteRecipe(){
        this.newRecipe = null;
    }
    deleteAllMedRecipe(){
        this.newMedicineinRecipe = [];
    }
}