import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { Doctor } from "../data/doctor.interface";

@Injectable()
export class DoctorService {
    private doctorList: Doctor[] = [];
    private url = 'http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/doctors/';
    constructor(
        private http: Http,
    ){}


    getAllDoctor() {
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }

    getDoctor(id) {
        return this.http.get(this.url + 'view/' + id).map(res => res.json());
    }
    
    addDoctor(doctor:Doctor){
        this.doctorList.push(doctor);
    }

    getDoctorList() {
        return this.doctorList;
    }

    clearDataList(){
        this.doctorList = [];
    }

}