import { Injectable } from '@angular/core';
import { Http,Headers, RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';
import { Recipe } from "../data/recipe.interface";

@Injectable()
export class RecipeService {
    private recipeList: any[] = [];
    private url = 'http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/recipes/';
    constructor(
        private http: Http,
    ){}

    getAllRecipe() {
        return this.http.get(this.url + 'view-all').map(res => res.json());
    }

    getMedList(id) {
        return this.http.get(this.url + 'view-medicine-list/' + id).map(res => res.json());
    }

    create(data: Recipe) {
        // console.log(data.genericName);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
            headers: headers
        });
        let body = 
            'doctor_id=' + data.doctor_id +
            '&patient_id=' + data.patient_id +
            '&pharmacist_id=' + data.pharmacist_id +
            '&description=' + data.description;

        return this.http.post(this.url + 'create', body, options)
            .toPromise()
            .then(
                response => {
                    console.log(response.json())
                }
            );
    }

    addRecipe(recipe:Recipe){
        this.recipeList.push(recipe);
    }

    getRecipeList() {
        return this.recipeList;
    }

    clearDataList(){
        this.recipeList = [];
    }
}